package com.kafka.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kafka.consumer.entity.PersonalInformationEntity;

public interface PersonalInformationRepository extends JpaRepository<PersonalInformationEntity, Long>{
	

}
