package com.kafka.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kafka.consumer.entity.AddressInformationEntity;

public interface AddressInformationRepository extends JpaRepository<AddressInformationEntity, Long>{

}
