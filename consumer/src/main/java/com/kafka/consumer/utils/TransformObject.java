package com.kafka.consumer.utils;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

@Component
public class TransformObject {
	public void copyProperties(Object source, Object destination) {
		  ModelMapper modelMapper = new ModelMapper();
	  modelMapper.map(source, destination); 
}

	public <T, I> void copyList(List<I> sourceList, List<T> destinationList) {
		ModelMapper modelMapper = new ModelMapper();
		Type listType = new TypeToken<List<I>>() {}.getType();
		sourceList = modelMapper.map(destinationList, listType);
	}

}
