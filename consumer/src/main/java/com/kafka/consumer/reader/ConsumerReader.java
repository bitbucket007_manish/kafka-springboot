package com.kafka.consumer.reader;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.kafka.consumer.service.PersonalInformationService;
/*--Controller is using only for CRUD operation*/

@Component
public class ConsumerReader {
	
	@Autowired
	private PersonalInformationService personalInformationService;
	
	@KafkaListener(topics = {"test-topic"})
    public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws Exception {
        System.out.println("ConsumerRecord : "+ consumerRecord);
        personalInformationService.processPersonalInformation(consumerRecord);
    }

}
