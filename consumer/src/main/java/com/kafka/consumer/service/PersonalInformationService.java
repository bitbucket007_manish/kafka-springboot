package com.kafka.consumer.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.consumer.entity.AddressInformationEntity;
import com.kafka.consumer.entity.PersonalInformationEntity;
import com.kafka.consumer.event.KafkaEvent;
import com.kafka.consumer.repository.AddressInformationRepository;
import com.kafka.consumer.repository.PersonalInformationRepository;
import com.kafka.consumer.utils.TransformObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PersonalInformationService {
	
	@Autowired
	private TransformObject transformObject;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private PersonalInformationRepository personalInformationRepository;
	
	@Autowired
	private AddressInformationRepository addressInformationRepository;

	public void processPersonalInformation(ConsumerRecord<Integer, String> consumerRecord)
			throws JsonMappingException, JsonProcessingException, Exception {

		KafkaEvent kafkaEvent = objectMapper.readValue(consumerRecord.value(), KafkaEvent.class);
		PersonalInformationEntity personalInformationEntity = new PersonalInformationEntity();
		transformObject.copyProperties(kafkaEvent.getPerson().getPersonalInformation(), personalInformationEntity);
		personalInformationRepository.save(personalInformationEntity);
		log.info("PersonalInformation Data Saved in DB");
		
		AddressInformationEntity addressInformationEntity = new AddressInformationEntity();
		addressInformationEntity.setPersonalInformation(personalInformationEntity);
		transformObject.copyProperties(kafkaEvent.getPerson().getAddressInformation(), addressInformationEntity);
		addressInformationRepository.save(addressInformationEntity);
		log.info("AddressInformation Data Saved in DB");
	}
}
