package com.kafka.consumer.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="address_Information")
public class AddressInformationEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String state;
	private String city;
	private String pinCode;
	
	@ManyToOne()
	@JoinColumn(name = "fk_id")
	private PersonalInformationEntity personalInformation;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public PersonalInformationEntity getPersonalInformation() {
		return personalInformation;
	}
	public void setPersonalInformation(PersonalInformationEntity personalInformation) {
		this.personalInformation = personalInformation;
	}
}