package com.kafka.consumer.shared.io;

import java.io.Serializable;

public class AddressInformation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String state;
	private String city;
	private String pinCode;
	
	
	public AddressInformation() {
		super();
	}

	public AddressInformation(String state, String city, String pinCode) {
		super();
		this.state = state;
		this.city = city;
		this.pinCode = pinCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	@Override
	public String toString() {
		return "AddressInformation [state=" + state + ", city=" + city + ", pinCode=" + pinCode + "]";
	}
}
