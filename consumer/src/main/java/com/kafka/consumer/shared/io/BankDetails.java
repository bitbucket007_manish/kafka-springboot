package com.kafka.consumer.shared.io;

import java.io.Serializable;

public class BankDetails implements Serializable{
	
	private String bankName;
	private String accountNumber;
	
	
	public BankDetails() {
		super();
	}
	
	public BankDetails(String bankName, String accountNumber) {
		super();
		this.bankName = bankName;
		this.accountNumber = accountNumber;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Override
	public String toString() {
		return "BankDetails [bankName=" + bankName + ", accountNumber=" + accountNumber + "]";
	}
	
}
