package com.kafka.consumer.event;

import com.kafka.consumer.shared.io.Person;

public class KafkaEvent {
	private Integer kafkaEventId;
    private Person person;
    
	public Integer getKafkaEventId() {
		return kafkaEventId;
	}
	public void setKafkaEventId(Integer kafkaEventId) {
		this.kafkaEventId = kafkaEventId;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	@Override
	public String toString() {
		return "KafkaEvent [kafkaEventId=" + kafkaEventId + ", person=" + person + "]";
	}
}
