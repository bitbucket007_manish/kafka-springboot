package com.consumer.policy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.consumer.policy.entity.PolicyEntity;
import com.consumer.policy.shared.iO.BankDetails;

public interface PolicyRepository extends JpaRepository<PolicyEntity, Long>{
	PolicyEntity findByPolicyNumber(String policyNumber);
}
