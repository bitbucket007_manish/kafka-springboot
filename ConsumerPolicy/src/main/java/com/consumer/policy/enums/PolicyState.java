package com.consumer.policy.enums;

public enum PolicyState {
	CREATE, CONSUME, PREMIUM
}
