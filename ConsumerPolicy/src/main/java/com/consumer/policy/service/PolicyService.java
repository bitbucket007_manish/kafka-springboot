package com.consumer.policy.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.consumer.policy.entity.PolicyEntity;
import com.consumer.policy.event.PolicyEvent;
import com.consumer.policy.repository.PolicyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PolicyService {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private PolicyRepository policyRepository;
	
	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;
	

	public void processPolicyInformation(ConsumerRecord<Integer, String> consumerRecord)
			throws JsonMappingException, JsonProcessingException, Exception {
		PolicyEvent policyEvent = objectMapper.readValue(consumerRecord.value(), PolicyEvent.class);
		
		if(policyEvent.getPolicyNumber()!=null && policyEvent.getPolicyNumber().equalsIgnoreCase("unique")) {
			throw new RecoverableDataAccessException("DB Connection Failed! -- Network/Connection Issue");
		}
		
		if(policyEvent.getPolicyNumber()==null || policyEvent.getPolicyNumber().isEmpty()) {
			throw new IllegalArgumentException("Policy Number is missing");
		}
		
		switch(policyEvent.getPolicyState()) {

		case CREATE:{
			PolicyEntity policyEntity = initializePolicyEntity(policyEvent);
			policyRepository.save(policyEntity);
			break;
		}
		case CONSUME:{
			PolicyEntity existingPolicy = policyRepository.findByPolicyNumber(policyEvent.getPolicyNumber());
			if(existingPolicy!=null) {
				existingPolicy.setAmount(existingPolicy.getAmount() - policyEvent.getPerson().getBankDetails().getAmount());
				policyRepository.save(existingPolicy);
			}
			break;
		}case PREMIUM:{
			PolicyEntity existingPolicy = policyRepository.findByPolicyNumber(policyEvent.getPolicyNumber());
			if(existingPolicy!=null) {
				existingPolicy.setAmount(existingPolicy.getAmount() + policyEvent.getPerson().getBankDetails().getAmount());
				policyRepository.save(existingPolicy);
			}
			break;
		}
		default :
			log.info("default Switch Cases");
		}
	}
	
	public void handleRecoverty(ConsumerRecord<Integer, String> record) {
		Integer key = record.key();
		String message = record.value();
		ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.sendDefault(key,message);
		listenableFuture.addCallback(invokeCallback(key, message));
	}
	
	protected void handleSuccess(Integer key, String message, SendResult<Integer, String> result) {
		System.out.println("Message Sent Successfully for the key : " + key + " and the message is " + message
				+ " for partition is " + result.getRecordMetadata().partition());
	}

	protected void handleFailure(Integer key, String message, Throwable exception) {
		System.out.println("Error sending Message for the key : " + key + " and the message is " + message
				+ " for exception is" + exception.getMessage());
	}
	
	private ListenableFutureCallback<SendResult<Integer, String>> invokeCallback(Integer key, String message) {
		return new ListenableFutureCallback<SendResult<Integer, String>>() {

			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				handleSuccess(key, message, result);
			}

			@Override
			public void onFailure(Throwable exception) {
				handleFailure(key, message, exception);
			}
		};
	}

	private PolicyEntity initializePolicyEntity(PolicyEvent policyEvent) {
		PolicyEntity policyEntity = new PolicyEntity();
		policyEntity.setPolicyNumber(policyEvent.getPolicyNumber());
		policyEntity.setBankName(policyEvent.getPerson().getBankDetails().getBankName());
		policyEntity.setAccountNumber(policyEvent.getPerson().getBankDetails().getAccountNumber());
		policyEntity.setAmount(policyEvent.getPerson().getBankDetails().getAmount());
		return policyEntity;
	}
}
