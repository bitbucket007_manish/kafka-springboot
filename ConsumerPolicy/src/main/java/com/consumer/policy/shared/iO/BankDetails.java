package com.consumer.policy.shared.iO;

public class BankDetails{
	
	private Long id;
	private String bankName;
	private String accountNumber;
	private double amount;
	
	public BankDetails() {
		super();
	}
	
	public BankDetails(String bankName, String accountNumber) {
		super();
		this.bankName = bankName;
		this.accountNumber = accountNumber;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}
