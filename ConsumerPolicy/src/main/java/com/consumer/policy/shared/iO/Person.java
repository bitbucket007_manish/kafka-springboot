package com.consumer.policy.shared.iO;

public class Person {
	
	private BankDetails bankDetails;
	
	public BankDetails getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}

}
