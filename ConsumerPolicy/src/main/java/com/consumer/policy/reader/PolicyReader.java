package com.consumer.policy.reader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.consumer.policy.service.PolicyService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PolicyReader {
	
	@Autowired
	private PolicyService policyService;
	
	@KafkaListener(topics = {"policy"})
    public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws Exception {
		LocalDateTime instance = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        log.info("ConsumerRecord : "+formatter.format(instance) +"   "+ consumerRecord);
        policyService.processPolicyInformation(consumerRecord);
        
    }
}
