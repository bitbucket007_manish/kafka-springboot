package com.consumer.policy.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import com.consumer.policy.service.PolicyService;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableKafka
@Slf4j
public class PolicyConfig {
	
	@Autowired
	private PolicyService policyService;

	@Bean
	public ConcurrentKafkaListenerContainerFactory<?, ?> kafkaListenerContainerFactory(ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
			ConsumerFactory<Object, Object> kafkaConsumerFactory) {
		ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
		configurer.configure(factory, kafkaConsumerFactory);
		factory.setConcurrency(3);
		factory.setErrorHandler(((thrownException, data )->{
			log.info("Exception in consumerConfig is {} and the record is {}", thrownException.getMessage(), data);
		}));
		factory.setRetryTemplate(retryTemplate());
		factory.setRecoveryCallback(context -> {
			if(context.getLastThrowable().getCause() instanceof RecoverableDataAccessException) {
				log.info("Inside the recoverable logic");
				/*
				 * Arrays.asList(context.attributeNames()) .forEach(attributeName -> {
				 * log.info("Attribute name is "+attributeName);
				 * log.info("Attributle Value is "+context.getAttribute(attributeName)); });
				 */
				
				ConsumerRecord<Integer, String> consumerRecord =(ConsumerRecord<Integer, String>) context.getAttribute("record");
				policyService.handleRecoverty(consumerRecord);
			}
			else {
				log.info("Inside the non recoverable logic");
				throw new RuntimeException(context.getLastThrowable().getMessage());
			}
			return null;
		});
		return factory;
	}

	private RetryTemplate retryTemplate() {
		FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
		fixedBackOffPolicy.setBackOffPeriod(2000);
		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setBackOffPolicy(fixedBackOffPolicy);
		retryTemplate.setRetryPolicy(SimpleRetryPolicy());
		return retryTemplate;
	}

	private RetryPolicy SimpleRetryPolicy() {
		/* Simple way to Retry logic -1 */
		/*
		 * SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy();
		 * simpleRetryPolicy.setMaxAttempts(3);
		 */
		/* Another Way to Retry logic - 2 */
		Map<Class<? extends Throwable>, Boolean> map = new HashMap<>(); 
		map.put(IllegalArgumentException.class, false);
		map.put(RecoverableDataAccessException.class, true);
		SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy(3, map, true);
		return simpleRetryPolicy;
	}
}
