package com.consumer.policy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaConsumerPolicyApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaConsumerPolicyApplication.class, args);
	}

}
