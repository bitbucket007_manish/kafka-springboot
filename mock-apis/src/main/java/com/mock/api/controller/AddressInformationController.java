package com.mock.api.controller;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mock.api.enums.StatusCode;
import com.mock.api.io.response.GenericResponse;
import com.mock.api.mongo.collection.AddressInformation;
import com.mock.api.mongo.repository.AddressInformationRepo;

@RestController
@RequestMapping("/addressInformation")
public class AddressInformationController {

	@Autowired
	private AddressInformationRepo addressInformationRepo;

	@GetMapping("/{id}")
	public ResponseEntity<GenericResponse> findAddressInformation(@PathVariable Integer id){
		AddressInformation result = null;
		Optional<AddressInformation> optionalAddressInfo = addressInformationRepo.findById(id);
		if(optionalAddressInfo.isPresent()) {
			result = optionalAddressInfo.get();
		}
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, result));
	}
	
	@PostMapping
	public ResponseEntity<GenericResponse> insetAddressInformation(@RequestBody AddressInformation addressInformation){
		AddressInformation result = addressInformationRepo.save(addressInformation);
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, result));
	}
}
