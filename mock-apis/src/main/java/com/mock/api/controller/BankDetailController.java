package com.mock.api.controller;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mock.api.enums.StatusCode;
import com.mock.api.io.response.GenericResponse;
import com.mock.api.mongo.collection.AddressInformation;
import com.mock.api.mongo.collection.BankDetails;
import com.mock.api.mongo.collection.PersonalInformation;
import com.mock.api.mongo.repository.BankDetailsRepo;

@RestController
@RequestMapping("/bankDetails")
public class BankDetailController {

	@Autowired
	private BankDetailsRepo bankDetailsRepo;

	@GetMapping("/{id}")
	public ResponseEntity<GenericResponse> findBankDetails(@PathVariable Integer id){
		BankDetails result = null;
		Optional<BankDetails> optionalBankDetails = bankDetailsRepo.findById(id);
		if(optionalBankDetails.isPresent()) {
			result = optionalBankDetails.get();
		}
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, result));
	}

	@PostMapping
	public ResponseEntity<GenericResponse> demoJson(@RequestBody BankDetails bankDetails){
		BankDetails result = null;
		Optional<BankDetails> existingBankDetails = bankDetailsRepo.findById(bankDetails.getId());
		if(existingBankDetails.isPresent()){
			result = existingBankDetails.get();
		}
		else {
			result = bankDetailsRepo.save(bankDetails);	
		}
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, result));
	}
}
