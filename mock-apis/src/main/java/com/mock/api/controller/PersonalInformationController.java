package com.mock.api.controller;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mock.api.enums.StatusCode;
import com.mock.api.io.response.GenericResponse;
import com.mock.api.mongo.collection.AddressInformation;
import com.mock.api.mongo.collection.BankDetails;
import com.mock.api.mongo.collection.PersonalInformation;
import com.mock.api.mongo.repository.PersonalInformationRepo;

@RestController
@RequestMapping("/personalInformation")
public class PersonalInformationController {
	
	@Autowired
	private PersonalInformationRepo personalInformationRepo;
	
	@GetMapping("/{id}")
	public ResponseEntity<GenericResponse> findPersonalInformation(@PathVariable Integer id){
		PersonalInformation personalInformation = null;
		Optional<PersonalInformation> optionalPersonalInformation = personalInformationRepo.findById(id);
		if(optionalPersonalInformation.isPresent()) {
			personalInformation = optionalPersonalInformation.get();
		}
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, personalInformation));
	}
	
	@PostMapping
	public ResponseEntity<GenericResponse> insertPersonalInformation(@RequestBody PersonalInformation personalInformation){
		PersonalInformation result = personalInformationRepo.save(personalInformation);
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, result));
	}
	
}
