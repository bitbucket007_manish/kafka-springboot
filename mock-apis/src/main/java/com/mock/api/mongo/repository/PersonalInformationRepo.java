package com.mock.api.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mock.api.mongo.collection.PersonalInformation;

public interface PersonalInformationRepo extends MongoRepository<PersonalInformation, Integer> {

}
