package com.mock.api.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mock.api.mongo.collection.AddressInformation;

public interface AddressInformationRepo extends MongoRepository<AddressInformation, Integer> {
		

}
