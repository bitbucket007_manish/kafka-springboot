package com.mock.api.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mock.api.mongo.collection.BankDetails;

public interface BankDetailsRepo extends MongoRepository<BankDetails, Integer> {

}
