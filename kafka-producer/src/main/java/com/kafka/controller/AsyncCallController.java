package com.kafka.controller;

import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.kafka.enums.StatusCode;
import com.kafka.io.response.GenericResponse;
import com.kafka.service.PersonInformationIntf;

@RestController
public class AsyncCallController {
	
	@Autowired
	private PersonInformationIntf personInformationIntf;

	@GetMapping("/supply-async")
	public ResponseEntity<GenericResponse> supplyASyncCall()
			throws JSONException, JsonMappingException, JsonProcessingException, InterruptedException, ExecutionException {
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, personInformationIntf.getPersonInformation()));
	}
}
