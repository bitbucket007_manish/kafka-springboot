package com.kafka.controller;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.enums.StatusCode;
import com.kafka.event.KafkaEvent;
import com.kafka.event.PolicyEvent;
import com.kafka.io.response.GenericResponse;
import com.kafka.io.response.Person;
import com.kafka.io.shared.AddressInformation;
import com.kafka.io.shared.BankDetails;
import com.kafka.io.shared.PersonalInformation;
import com.kafka.producer.KafkaProducer;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PolicyController {

	@Autowired
	private KafkaProducer kafkaProducer;

	@PostMapping("/policy")
	public ResponseEntity<GenericResponse> processPolicy(@RequestBody PolicyEvent policyEvent){
		try {
			policyEvent.setPolicyEventId(new Random().nextInt());
			kafkaProducer.sendKafkaEventByPolicyRecord(policyEvent);
		} catch (JsonProcessingException ex) {
			log.error("Exception found in process policy event producing :", ex);
		}
		return ResponseEntity.ok(new GenericResponse(StatusCode.SUCCESS, null));
	}
}
