package com.kafka.producer;

import java.util.Arrays;
import java.util.List;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.constants.Constant;
import com.kafka.event.KafkaEvent;
import com.kafka.event.PolicyEvent;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class KafkaProducer {

	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;
	@Autowired
	private ObjectMapper objectMapper;


	public void sendKafkaEvent(KafkaEvent kafkaEvent) throws JsonProcessingException {
		Integer key = kafkaEvent.getKafkaEventId();
		String message = objectMapper.writeValueAsString(kafkaEvent);
		ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.send(Constant.TEST_TOPIC, key, message);
		listenableFuture.addCallback(invokeCallback(key, message));
	}

	public void sendKafkaEventByProducerRecord(KafkaEvent kafkaEvent) throws JsonProcessingException {
		Integer key = kafkaEvent.getKafkaEventId();
		String message = objectMapper.writeValueAsString(kafkaEvent);
		ProducerRecord<Integer, String> producerRecords = buildProducerRecords(Constant.TEST_TOPIC, key, message);
		ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.send(producerRecords);
		listenableFuture.addCallback(invokeCallback(key, message));
	}
	public void sendKafkaEventByPolicyRecord(PolicyEvent policyEvent) throws JsonProcessingException {
		Integer key = policyEvent.getPolicyEventId();
		String message = objectMapper.writeValueAsString(policyEvent);
		ProducerRecord<Integer, String> producerRecords = buildProducerRecords(Constant.POLICY_TOPIC, key, message);
		ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.send(producerRecords);
		listenableFuture.addCallback(invokeCallback(key, message));
	}

	private ListenableFutureCallback<SendResult<Integer, String>> invokeCallback(Integer key, String message) {
		return new ListenableFutureCallback<SendResult<Integer, String>>() {

			@Override
			public void onSuccess(SendResult<Integer, String> result) {
				handleSuccess(key, message, result);
			}

			@Override
			public void onFailure(Throwable exception) {
				handleFailure(key, message, exception);
			}
		};
	}

	protected void handleSuccess(Integer key, String message, SendResult<Integer, String> result) {
		log.info("Message Sent Successfully for the key : " + key + " and the message is " + message
				+ " for partition is " + result.getRecordMetadata().partition());
	}

	protected void handleFailure(Integer key, String message, Throwable exception) {
		log.info("Error sending Message for the key : " + key + " and the message is " + message
				+ " for exception is" + exception.getMessage());
	}

	protected ProducerRecord<Integer, String> buildProducerRecords(String topic, Integer key, String message) {
		RecordHeader recordHeader = new RecordHeader(key.toString(), message.getBytes());
		List<Header> listRecordHeader = Arrays.asList(recordHeader);
		return new ProducerRecord<Integer, String>(topic, null, key, message, listRecordHeader);
	}

}
