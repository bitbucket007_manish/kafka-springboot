package com.kafka.service;

import java.util.concurrent.ExecutionException;

import com.kafka.io.response.Person;

public interface PersonInformationIntf {
	
	public Person getPersonInformation() throws InterruptedException, ExecutionException;

}
