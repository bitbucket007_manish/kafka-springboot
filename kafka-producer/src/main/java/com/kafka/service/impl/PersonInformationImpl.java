package com.kafka.service.impl;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.event.KafkaEvent;
import com.kafka.io.response.GenericResponse;
import com.kafka.io.response.Person;
import com.kafka.io.shared.AddressInformation;
import com.kafka.io.shared.BankDetails;
import com.kafka.io.shared.PersonalInformation;
import com.kafka.producer.KafkaProducer;
import com.kafka.service.PersonInformationIntf;
import com.kafka.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PersonInformationImpl implements PersonInformationIntf{

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private KafkaProducer kafkaProducer;

	@Autowired
	private CommonUtil commonUtil;

	@Override
	public Person getPersonInformation() throws InterruptedException, ExecutionException {

		Person person = new Person();
		CompletableFuture<PersonalInformation> futurePersonalInformation = CompletableFuture.supplyAsync(() -> {
			PersonalInformation personalInformation = null;
			try {
				personalInformation = personalInformation();
			} catch (JsonProcessingException | JSONException e) {
				log.error("Exception occoured on processing personalInformation Call");
			}
			return personalInformation;
		});

		CompletableFuture<AddressInformation> futureAddressInformation = CompletableFuture.supplyAsync(() ->{
			AddressInformation addressInformation = null;
			try {
				addressInformation = addressInformation();
			} catch (JsonProcessingException | JSONException e) {
				log.error("Exception occoured on processing addressInformation Call");
			}
			return addressInformation;
		});

		CompletableFuture<BankDetails> futureBankDetails = CompletableFuture.supplyAsync(()->{
			BankDetails bankDetails = null;
			try {
				bankDetails = bankInformation();
			} catch (Exception e) {
				log.error("Exception occoured on processing bankInformation Call");
			}
			return bankDetails;
		});
		PersonalInformation personalInformation = futurePersonalInformation.get();
		AddressInformation addressInformation = futureAddressInformation.get();
		BankDetails bankDetails = futureBankDetails.get();
		person.setPersonalInformation(personalInformation);
		person.setAddressInformation(addressInformation);
		person.setBankDetails(bankDetails);

		//------ Kafka Implementation-------------------
		CompletableFuture.runAsync(()->{
			try {
				KafkaEvent event = new KafkaEvent();
				event.setKafkaEventId(new Random().nextInt());
				event.setPerson(person);
				/* Producing Record - Way 1 */
				//kafkaProducer.sendKafkaEvent(event);
				/* Producing Record - Way 2 */
				kafkaProducer.sendKafkaEventByProducerRecord(event);
			} catch (JsonProcessingException ex) {
				log.error("exception found in kafka event producing :", ex);
			}
		});
		return person;
	}
	@SuppressWarnings("unused")
	private PersonalInformation personalInformation() throws JSONException, JsonProcessingException, JsonMappingException {
		final String personalInformationUrl = "http://localhost:8083/personalInformation/1";
		JSONObject jsonObject = commonUtil.restTemplateCall(personalInformationUrl);
		String str = jsonObject.getString("result");
		log.info("Personal Information :-{}", str);
		PersonalInformation per = objectMapper.readValue(str, PersonalInformation.class);
		return per;
	}

	@SuppressWarnings("unused")
	private AddressInformation addressInformation() throws JSONException, JsonProcessingException, JsonMappingException {
		final String addressInformationUrl = "http://localhost:8083/addressInformation/123";
		JSONObject jsonObject = commonUtil.restTemplateCall(addressInformationUrl);
		String str = jsonObject.getString("result");
		log.info("Address Information :- {}", str);
		AddressInformation addressInformation = objectMapper.readValue(str, AddressInformation.class);
		return addressInformation;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	private BankDetails bankInformation() throws JSONException, JsonProcessingException, JsonMappingException {
		final String bankDetailsUrl = "http://localhost:8083/bankDetails";
		BankDetails bankDetails = new BankDetails(1,"HDFC Bank", "79456456456", 100000);

		HttpHeaders headers=new HttpHeaders();
		headers.set("Content-Type", "application/json");
		@SuppressWarnings("rawtypes")
		HttpEntity requestEntity=new HttpEntity(bankDetails, headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<GenericResponse> response = restTemplate.exchange(bankDetailsUrl, HttpMethod.POST,requestEntity,GenericResponse.class);
		log.info("Response"+response);
		GenericResponse genericResponse = (GenericResponse) response.getBody();
		String result = objectMapper.writeValueAsString(genericResponse);
		JSONObject jsonObject = new JSONObject(result);
		String str = jsonObject.getString("result");
		log.info("Bank Details :- {} ", str);
		BankDetails bankDetails1 = objectMapper.readValue(str, BankDetails.class);
		return bankDetails1;
	}
}
