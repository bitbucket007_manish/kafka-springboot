package com.kafka.event;

import com.kafka.enums.PolicyState;
import com.kafka.io.response.Person;

public class PolicyEvent {
	
	private Integer policyEventId;
	private String policyNumber;
    private Person person;
    private PolicyState policyState;
    
	public Integer getPolicyEventId() {
		return policyEventId;
	}
	public void setPolicyEventId(Integer policyEventId) {
		this.policyEventId = policyEventId;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public PolicyState getPolicyState() {
		return policyState;
	}
	public void setPolicyState(PolicyState policyState) {
		this.policyState = policyState;
	}
}
