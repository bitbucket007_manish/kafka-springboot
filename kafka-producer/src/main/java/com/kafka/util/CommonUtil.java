package com.kafka.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CommonUtil {
	
	public JSONObject restTemplateCall(String url) throws JSONException {
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(url, String.class);
		JSONObject jsonObject = new JSONObject(result);
		return jsonObject;
	}
}
