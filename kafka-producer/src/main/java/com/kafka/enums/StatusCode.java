package com.kafka.enums;

public enum StatusCode {

	SUCCESS(0, "Success"), FAIL(-1, "Failure");

	private int code;
	private String message;

	private StatusCode(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}

