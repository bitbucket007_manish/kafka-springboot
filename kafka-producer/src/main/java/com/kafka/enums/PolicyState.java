package com.kafka.enums;

public enum PolicyState {
	CREATE, CONSUME, PREMIUM
}
