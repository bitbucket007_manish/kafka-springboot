package com.kafka.io.shared;

import java.io.Serializable;

import lombok.Data;

@Data
public class AddressInformation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String state;
	private String city;
	private String pinCode;
	
	
	
}
