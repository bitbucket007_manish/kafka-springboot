package com.kafka.io.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankDetails{
	
	private Integer id;
	private String bankName;
	private String accountNumber;
	private double amount;
	
	
}
