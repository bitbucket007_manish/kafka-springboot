package com.kafka.io.shared;

import java.io.Serializable;

import lombok.Data;

@Data
public class PersonalInformation implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private String mobile;
	private String email;
	
}
