package com.kafka.io.response;

import com.kafka.enums.StatusCode;

public class GenericResponse {
	private Integer status;
	private String message;
	private Object result;
	private String errorCode;
	private long pagableCount;
	
	
	public GenericResponse() {
		super();
	}
	
	public GenericResponse(String message) {
		super();
		this.message = message;
	}

	public GenericResponse(StatusCode status, Object result, long pagableCount) {
		super();
		this.status = status.getCode();
		this.message = status.getMessage();
		this.result = result;
		this.pagableCount=pagableCount;
	}
	
	public GenericResponse(StatusCode status, Object result) {
		super();
		this.status = status.getCode();
		this.message = status.getMessage();
		this.result = result;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public long getPagableCount() {
		return pagableCount;
	}

	public void setPagableCount(long pagableCount) {
		this.pagableCount = pagableCount;
	}

}
