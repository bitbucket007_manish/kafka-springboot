package com.kafka.io.response;

import com.kafka.enums.PolicyState;
import com.kafka.io.shared.AddressInformation;
import com.kafka.io.shared.BankDetails;
import com.kafka.io.shared.PersonalInformation;

public class Person {
	
	private PersonalInformation personalInformation;
	private AddressInformation addressInformation;
	private BankDetails bankDetails;
	
	public PersonalInformation getPersonalInformation() {
		return personalInformation;
	}
	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}
	public AddressInformation getAddressInformation() {
		return addressInformation;
	}
	public void setAddressInformation(AddressInformation addressInformation) {
		this.addressInformation = addressInformation;
	}
	public BankDetails getBankDetails() {
		return bankDetails;
	}
	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}
	@Override
	public String toString() {
		return "Person [personalInformation=" + personalInformation + ", addressInformation=" + addressInformation
				+ ", bankDetails=" + bankDetails + "]";
	}
}
