package com.crud.models;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Document(collection = "customer")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerCollection {

	private String id;
	private String firstName;
	private String lastName;
	private int age;
	private Address address;
	
	
	
	

}
