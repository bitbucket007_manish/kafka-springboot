package com.crud.repos;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.crud.models.CustomerCollection;

public interface CustomerRepository extends MongoRepository<CustomerCollection, String> {

	List<CustomerCollection> findByFirstName(String firstName);

	List<CustomerCollection> findByAddressZipCode(int zipcode);

	@Query(value = "{'age' : {$gte:?0}}")
	List<CustomerCollection> anyFunctionName(int age);



}
