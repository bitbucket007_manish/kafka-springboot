package com.crud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crud.models.CustomerCollection;
import com.crud.repos.CustomerRepository;
import com.crud.service.CustomerService;

@RestController
@RequestMapping("/api")
public class ApiController {

	@Autowired
	private CustomerService CustomerService;

	@PostMapping("/customer")
	public CustomerCollection test(@RequestBody CustomerCollection customerCollection) {
		return CustomerService.saveCustomer(customerCollection);
	}

	@GetMapping("/{id}")
	public CustomerCollection findByName(@PathVariable String id) {
		Optional<CustomerCollection> findByName = CustomerService.getById(id);
		return findByName.get();
	}

	@GetMapping
	public List<CustomerCollection> findAllCustomer() {
		return CustomerService.findAll();
	}
	
	@GetMapping("/example")
	public List<CustomerCollection> getAllByExample(@RequestBody CustomerCollection customerCollection) {
		return CustomerService.getAllByExample(customerCollection);
	}
	
	@GetMapping("/name/{firstName}")
	public List<CustomerCollection> findByFirstName(@PathVariable String firstName) {
		List<CustomerCollection> findByFirstName = CustomerService.findByFirstName(firstName);
		return findByFirstName;
	}
	
	@GetMapping("/zipcode")
	public List<CustomerCollection> findByZipcode(@RequestParam(name="zipcode") int zipcode) {
		List<CustomerCollection> findByZipCode = CustomerService.findByZipCode(zipcode);
		return findByZipCode;
	}
	
	@GetMapping("/age")
	public List<CustomerCollection> findByAgeGreaterThan(@RequestParam(name="age") int age) {
		List<CustomerCollection> findByAge = CustomerService.findByAgeGreaterThan(age);
		return findByAge;
	}

}
