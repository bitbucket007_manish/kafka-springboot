package com.crud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.crud.models.CustomerCollection;
import com.crud.repos.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;


	public CustomerCollection saveCustomer(CustomerCollection customerCollection) {
		return customerRepository.insert(customerCollection);
	}

	public Optional<CustomerCollection> getById(String id) {
		return customerRepository.findById(id);
	}

	public List<CustomerCollection> findAll() {
		return customerRepository.findAll();
	}

	public List<CustomerCollection> getAllByExample(CustomerCollection customerCollection) {
		Example<CustomerCollection> e = Example.of(customerCollection);
		return customerRepository.findAll(e);
	}
	public List<CustomerCollection> findByFirstName(String firstName) {
		return customerRepository.findByFirstName(firstName);
	}

	public List<CustomerCollection> findByZipCode(int zipcode) {
		return customerRepository.findByAddressZipCode(zipcode);
	}

	public List<CustomerCollection> findByAgeGreaterThan(int age) {
		return customerRepository.anyFunctionName(age);
	}




}
